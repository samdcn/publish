# publish

#### 介绍
程序公开发布地址

#### 软件架构
软件架构说明
Vs2019 Vb.net html js jquery 使用cefsharp作为UI,异步事件驱动前后端搭的框架


#### 安装教程

1.  需要windows7的系统
2.  需要安装.net 5
2.  在vs中包管理器中打开项目后自行添加cefsharp.common.core 和 cefsharp.winform.core，当前使用版本号100

#### 使用说明

使用vs.net 2019整理编写
里面含有部分注解

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
